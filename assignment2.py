from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON')
def bookJSON():
    r = json.dumps(books)
    val = app.make_response(r)
    return val

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showPage.html',booklist=books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        newName = request.form['newName']
        newindex = 1
        for entry in books:
            if int(entry['id']) > newindex:
                newindex = int(entry['id']) + 1
        books.append({'title': newName, 'id': str(newindex)})
        return redirect(url_for('showBook'))
    return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == 'POST':
        newName = request.form['newName']
        for entry in books:
            if entry['id'] == str(book_id):
                entry['title'] = newName
                break
        return redirect(url_for('showBook'))
    return render_template('editBook.html')
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'POST':
        for i in range(len(books)):
            entry = books[i]
            if entry['id'] == str(book_id):
                books.pop(i)
                break
        return redirect(url_for('showBook'))
    return render_template('deleteBook.html')

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

